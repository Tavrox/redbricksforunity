﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsItem : MonoBehaviour
{
    internal Image Picture;
    internal Text Title, Description;

    private void Start()
    {
        Picture = transform.Find("Picture").GetComponent<Image>();
        Title = transform.Find("Title").GetComponent<Text>();
        Description = transform.Find("Description").GetComponent<Text>();
    }

    internal void Load(EntryStructure _entry)
    {
        print("loading");
    }
}
