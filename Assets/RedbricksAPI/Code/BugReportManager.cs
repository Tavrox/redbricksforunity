﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BugReportManager : MonoBehaviour
{
    [Header("Key triggering bug report")]
    public KeyCode Key = KeyCode.F12;

    GameObject bugReport, instanceReport;

    // Instance
    Button Butt;

    // Start is called before the first frame update
    void Start()
    {
        bugReport = Resources.Load("BugReporter") as GameObject;
        if (transform.Find("BugReporter").GetComponentInChildren<CanvasGroup>() == null) 
        { // Case where the object is already here

        }
    }

    void Update()
    {
        print("update");
        if (Input.GetKeyDown(Key))
        {
            print("show");
            showReporter();
        }
    }

    void showReporter()
    {
        if (instanceReport == null)
        {
            print("instantiate");
            instanceReport = Instantiate(bugReport, transform, false);
            Butt = instanceReport.GetComponentInChildren<Button>();
            Butt.onClick.AddListener(() => Send());
        }
        else
        {
            instanceReport.gameObject.SetActive(true);
        }
    }

    void Send()
    {
        instanceReport.gameObject.SetActive(false);
    }

    void Hide()
    {

    }
}
