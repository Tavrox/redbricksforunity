﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* One survey has several proposal. Every porposal is a voting linked to a specific user.
 * 
 * 
 */

public class VotingStruct
{
    internal string Id;
    internal string userId;
    internal string surveyId;
    internal string proposalId;

    public VotingStruct()
    {

    }
}
public class SurveyStruct
{
    internal string Id;
    internal string projectId;
    internal string userId;
    internal string question;
    internal DateTime createdAt;
    internal DateTime deadline;

    public SurveyStruct(string _quest)
    {
        question = _quest;
        createdAt = DateTime.Now;
        deadline = DateTime.Now.AddMonths(1);
    }
}

public class ProposalStruct
{
    internal string Id;
    internal string surveyId;

    internal string Proposal; // text
    internal int Image;

    public ProposalStruct(string _surveyID, string _txt, int _img)
    {
        Proposal = _txt;
    }
}