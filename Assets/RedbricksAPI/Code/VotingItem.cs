﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VotingItem : Toggle
{
    Image Pic;
    TextMeshProUGUI Parag;

    // Gather components
    internal void Setup()
    {
        Pic = transform.Find("Image").GetComponent<Image>();
        Parag = transform.Find("Label").GetComponent<TextMeshProUGUI>();
    }

    // Assign data taken from DB
    internal void Assign(ProposalStruct _proposal)
    {
        Parag.text = _proposal.Proposal;
    }
}
