﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public struct FeedStructure
{
    public string category;
    public string updated;
    public string title;
}

public struct EntryStructure
{
    public string title;
    public string author;
    public string content;
    public string link;
}

public class NewsGetter : MonoBehaviour
{
    XmlTextReader rssReader;
    XmlDocument rssDoc;
    List<EntryStructure> Entries = new List<EntryStructure>();
    NewsItem Model;
    
    void Start()
    {
        
        Model = GetComponentInChildren<NewsItem>();
        foreach (EntryStructure _entry in readNews("http://www.reddit.com/r/goblinzstudio/.rss"))
        {
            NewsItem _news = Instantiate(Model, Model.transform.parent) as NewsItem;
            _news.Load(_entry);
        }
    }

    internal List<EntryStructure> readNews(string _url)
    {
        rssReader = new XmlTextReader(_url);
        rssDoc = new XmlDocument();
        rssDoc.Load(rssReader);

        FeedStructure _feed = new FeedStructure();
        _feed.category = rssDoc["feed"]["category"].GetAttributeNode("term").InnerText;
        for (int i = 0; i < rssDoc["feed"].ChildNodes.Count; i++)
        {
            //Debug.Log(rssDoc["feed"].ChildNodes[i].Name);
            // If it is the rss tag
            if (rssDoc["feed"].ChildNodes[i].Name == "entry")
            {
                EntryStructure _struc = new EntryStructure();
                _struc.title = rssDoc["feed"].ChildNodes[i]["title"].InnerText;
                //print(_struc.title);
                Entries.Add(_struc);
            }
        }
        return Entries;
    }
}
