﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VotingManager : MonoBehaviour
{
    TextMeshProUGUI Header, Description;
    List<VotingItem> Votes = new List<VotingItem>();

    VotingStruct Voting;
    SurveyStruct Survey;
    List<ProposalStruct> Proposal = new List<ProposalStruct>();

    // Start is called before the first frame update
    void Start()
    {
        Description = transform.Find("Description").GetComponent<TextMeshProUGUI>();
        List<Toggle> Togs = GetComponentsInChildren<Toggle>().ToList();

        foreach (Toggle _tog in Togs)
        {
            print("one");
            Votes.Add(_tog as VotingItem);
        }

        for (int i = 0; i < Votes.Count; i++)
        {
            Votes[i].Setup();
        }

        sampleData();
        feedSurvey();
    }

    void sampleData()
    {
        Debug.LogWarning("Voting - Add Sample Data");
        Survey = new SurveyStruct("What is this question?");
        Proposal.Add(new ProposalStruct(Survey.Id, "Option 1", 0)); 
        Proposal.Add(new ProposalStruct(Survey.Id, "Option 2", 0)); 
        Proposal.Add(new ProposalStruct(Survey.Id, "Option 3", 0)); 
    }

    void feedSurvey()
    {
        Debug.LogWarning("Voting - Feed Survey");
        if (Votes.Count != Proposal.Count)
        {
            Debug.LogError("Voting - Not enough proposals");
            return;
        }

        for (int i = 0; i < Votes.Count; i++)
        {
            Votes[i].Assign(Proposal[i]);
        }
    }
}
